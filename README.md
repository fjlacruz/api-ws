# App websocket con lambda AWS

Creacion del api gateway

| | |
|---|---|
|![Alt text](img/1.png) |![Alt text](img/2.png) |
|![Alt text](img/3.png) |![Alt text](img/4.png) |
|![Alt text](img/5.png) |![Alt text](img/6.png) |
|![Alt text](img/7.png) |![Alt text](img/8.png) |
|![Alt text](img/9.png) | |


# Test Online

Accede a la página de pruebas en línea [aquí](https://livepersoninc.github.io/ws-test-page/).

## URL del WebSocket

wss://4yoi8zazgh.execute-api.us-east-1.amazonaws.com/production/




## Comandos por Consola

Para conectarte al WebSocket usando `wscat`, ejecuta el siguiente comando:

npm i wscat

wscat -c wss://4yoi8zazgh.execute-api.us-east-1.amazonaws.com/production/




## JSON para Testear

### Enviar un Mensaje Público

```json
{
  "action": "sendPublic"
}

{
  "action": "sendPrivate"
}


{
  "action": "setName",
  "name": "bob"
}

{
  "members": ["bob"]
}
```

