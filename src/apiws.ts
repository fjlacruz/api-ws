// Importar AWS SDK usando la sintaxis de módulo ES
import { ApiGatewayManagementApiClient, PostToConnectionCommand } from "@aws-sdk/client-apigatewaymanagementapi";

const ENDPOINT = 'https://4yoi8zazgh.execute-api.us-east-1.amazonaws.com/production/';
const client = new ApiGatewayManagementApiClient({ endpoint: ENDPOINT });

interface BodyProps {
  name?: string;
  systemMessage?: string;
  members?: string[];
  publicMessage?: string;
  privateMessage?: string;
  message?: string;
  to?: string;
}

const names = {};

const sendToOne = async (id, body) => {
  try {
    await client.send(new PostToConnectionCommand({
      ConnectionId: id,
      Data: Buffer.from(JSON.stringify(body)),
    }));
  } catch (err) {
    console.error(err);
  }
};

const sendToAll = async (ids, body) => {
  const all = ids.map(i => sendToOne(i, body));
  return Promise.all(all);
};

export const handler = async (event: any) => {
  if (event.requestContext) {
    const { connectionId, routeKey } = event.requestContext;
    let body: BodyProps = {};
    try {
      if (event.body) {
        body = JSON.parse(event.body);
      }
    } catch (err) {
      console.log(err);
      return;
    }

    switch (routeKey) {
      case '$connect':
        // Implementación del caso $connect
        break;
      case '$disconnect':
        await sendToAll(Object.keys(names), { systemMessage: `${names[connectionId]} has left the chat` });
        delete names[connectionId];
        await sendToAll(Object.keys(names), { members: Object.values(names) });
        break;

      case 'setName':
        names[connectionId] = body.name || '';
        await sendToAll(Object.keys(names), { members: Object.values(names) });
        await sendToAll(Object.keys(names), { systemMessage: `${names[connectionId]} has joined the chat` });
        break;

      case 'sendPublic':
        await sendToAll(Object.keys(names), { publicMessage: `${names[connectionId]}: ${body.message}` });
        break;
      case 'sendPrivate':
        const recipient = Object.keys(names).find(name => names[name] === body.to) || '';
        await sendToOne(recipient, { privateMessage: `${names[connectionId]}: ${body.message}` });
        break;
      default:
        // Implementación por defecto
        break;
    }
  }

  // Implementación de la respuesta
  const response = {
    statusCode: 200,
    
  };
  return response;
};
